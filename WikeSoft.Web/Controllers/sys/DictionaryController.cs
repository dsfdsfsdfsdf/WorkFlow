﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web.Mvc;

using WikeSoft.Enterprise.Interfaces.Sys;
using WikeSoft.Enterprise.Models.Filters.Sys;
using WikeSoft.Enterprise.Models.Sys;

namespace WikeSoft.Web.Controllers.Sys
{
    public class DictionaryController : BaseController
    {
        private readonly IDataDictionaryService _dataDictionaryService;
        public DictionaryController(IDataDictionaryService dataDictionaryService)
        {
            _dataDictionaryService = dataDictionaryService;
        }

        // GET: Dictionary

        public ActionResult Index()
        {
            return View();
        }

        /// <summary>
        /// 添加
        /// </summary>
        /// <returns></returns>

        public ActionResult Add()
        {
            return View();
        }

        /// <summary>
        /// 添加
        /// </summary>
        /// <param name="model"></param>
        /// <returns></returns>
        [HttpPost]
        public ActionResult Add(DataDictionayAddModel model)
        {
            if (ModelState.IsValid)
            {
                var success = _dataDictionaryService.Add(model);
                if (success)
                    return RedirectToAction("Index");
            }
            return View(model);
        }

        /// <summary>
        /// 编辑
        /// </summary>
        /// <returns></returns>

        public ActionResult Edit(string id)
        {
            var model = _dataDictionaryService.Find(id);
            return View(model);
        }

        /// <summary>
        /// 编辑
        /// </summary>
        /// <param name="model"></param>
        /// <returns></returns>
        [HttpPost]
        public ActionResult Edit(DataDictionayEditModel model)
        {
            if (ModelState.IsValid)
            {
                var success = _dataDictionaryService.Edit(model);
                if (success)
                    return RedirectToAction("Index");
            }
            return View(model);
        }

        /// <summary>
        /// 判断字典名是否存在
        /// </summary>
        /// <returns></returns>
        public ActionResult IsExists(string Id, string DictionayName)
        {
            var exists = _dataDictionaryService.IsExists(Id, DictionayName);
            return JsonOk(!exists);
        }

        /// <summary>
        /// 获取列表
        /// </summary>
        /// <param name="filter"></param>
        /// <returns></returns>
        [HttpGet]
        public JsonResult GetList(DataDictionaryFilter filter)
        {
            var result = _dataDictionaryService.Query(filter);
            return JsonOk(result);
        }

        /// <summary>
        /// 分组
        /// </summary>
        /// <returns></returns>
        [ChildActionOnly]
        public PartialViewResult Groups()
        {
            var filter = new DataDictionaryFilter
            {
                page = 1,
                rows = int.MaxValue
            };
            var result = _dataDictionaryService.Query(filter);
            var list = result.rows;
            var group = list.GroupBy(c => c.GroupCode).Where(c => c.Any()).Select(s => new DataDictionayModel
            {
                GroupCode = s.First().GroupCode,
                GroupName = s.First().GroupName
            }).OrderBy(x=>x.GroupCode).ToList();

            return PartialView(group);
        }

        /// <summary>
        /// 删除用户
        /// </summary>
        /// <param name="ids"></param>
        /// <returns></returns>
        [HttpPost]
        public ActionResult Delete(IList<string> ids)
        {
            var success = _dataDictionaryService.Delete(ids);
            return success ? Ok() : Fail();
        }

    }
}