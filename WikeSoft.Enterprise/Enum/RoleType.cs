﻿using System.ComponentModel;

namespace WikeSoft.Enterprise.Enum
{
    /// <summary>
    /// 角色类型
    /// </summary>
    public enum RoleType
    {
        /// <summary>
        /// 用户角色
        /// </summary>
        [Description("用户角色")]
        User = 1,

        /// <summary>
        /// 页面角色
        /// </summary>
        [Description("页面角色")]
        Page = 2
    }
}
