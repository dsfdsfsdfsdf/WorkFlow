﻿using System.Web;
using System.Web.Mvc;
using Microsoft.AspNet.Identity;

using WikeSoft.Data.Models;

namespace WikeSoft.Web
{
    public class LogAttribute: ActionFilterAttribute
    {
        private readonly string _perationText;

        public LogAttribute(string operationText)
        {
            _perationText = operationText;
        }

        public override void OnActionExecuting(ActionExecutingContext filterContext)
        {
            
            base.OnActionExecuting(filterContext);
        }

        public override void OnActionExecuted(ActionExecutedContext filterContext)
        {
           
            base.OnActionExecuted(filterContext);
        }
    }
}